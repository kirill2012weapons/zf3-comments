<?php

namespace Comments\Controller;

use Comments\Controller\AbsController\AbstractController;
use Comments\Models\Entity\Comment;
use Comments\Service\CommentFactory\CommentModelFactory;
use Comments\Service\CommentService;
use Comments\Service\CommentViewService;
use Comments\Service\HelloService;
use Doctrine\ORM\EntityRepository;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;

class CommentController extends AbstractController
{

    public function indexAction()
    {
        /**
         * @var $commentService CommentService
         */
        return new ViewModel([
            'comments' => $this
                        ->getServiceManager()
                        ->get('comment_service'),
        ]);
    }
}
