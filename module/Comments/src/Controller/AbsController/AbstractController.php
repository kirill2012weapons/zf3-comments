<?php

namespace Comments\Controller\AbsController;

use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceManager;

class AbstractController extends AbstractActionController
{

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * @param $serviceManager
     * @return $this
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    /**
     * @param $em
     * @return $this
     */
    public function setEntityManager($em)
    {
        $this->em = $em;
        return $this;
    }

}