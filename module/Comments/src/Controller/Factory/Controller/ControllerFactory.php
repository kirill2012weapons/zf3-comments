<?php
/**
 * Created by PhpStorm.
 * User: kiril
 * Date: 29.03.2019
 * Time: 16:50
 */

namespace Comments\Controller\Factory\Controller;


use Comments\Controller\AbsController\AbstractController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var $service AbstractController
         */
        $service = (null === $options) ? new $requestedName : new $requestedName($options);

        $em = $container->get('doctrine.entitymanager.orm_default');

        $service->setEntityManager($em);

        return $service->setServiceManager($container);
    }

}