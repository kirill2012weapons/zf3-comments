<?php

namespace Comments\SymfonyForms;


use Comments\Models\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentSendForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', IntegerType::class, [
                'mapped' => false
            ])
            ->add('user_name', null, [
                'trim' => true,
            ])
            ->add('email', RepeatedType::class, [
                'invalid_message' => 'Email and Repeater Email must be identical',
            ])
            ->add('comment', null, [
                'trim' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }

}