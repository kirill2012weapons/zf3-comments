<?php

namespace Comments\Service;


use Comments\SymfonyForms\CommentSendForm;
use Symfony\Component\Form\FormFactory;
use Zend\View\Helper\HeadLink;
use Zend\View\Helper\HeadScript;
use Symfony\Component\HttpFoundation\Request;

class CommentViewService
{

    const PATH_TO_VIEW_COMMENT = __DIR__ . './../../view/comments/comments_template/comment_template.phtml';

    /**
     * @var HeadScript
     */
    private $headScripts;

    /**
     * @var HeadLink
     */
    private $headLink;

    /**
     * @var FormFactory
     */
    private $formBuilder;

    /**
     * @var Request
     */
    private $requestSy;

    private $comments = [];

    public function __construct(HeadScript $headScript, HeadLink $headLink, $formBuilder, $requestSy)
    {
        $this->formBuilder = $formBuilder;
        $this->headScripts = $headScript;
        $this->headLink = $headLink;
        $this->requestSy = $requestSy;
    }

    public function getFormBuilder()
    {
        return $this->formBuilder;
    }

    public function getSyRequest()
    {
        return $this->requestSy;
    }

    public function renderViewComment(array $comments, $countsComment, $form, $parentEntity)
    {
        /**
         * Path to the template comments
         */
        $this->comments = $comments;
        ob_start();
        include(self::PATH_TO_VIEW_COMMENT);
        $commentTemplate = ob_get_contents();
        ob_end_clean();
        echo $commentTemplate;
    }

    public function haveComments()
    {
        if (empty($this->comments)) return false;
        return true;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function setJs($path)
    {
        $hj = $this->getHeadScripts();
        $hj->appendFile($path, 'text/javascript');
    }

    public function setCss($path)
    {
        $hl = $this->getHeadLink();
        $hl->appendStylesheet($path);
    }

    public function getHeadScripts()
    {
        return $this->headScripts;
    }

    public function getHeadLink()
    {
        return $this->headLink;
    }

}