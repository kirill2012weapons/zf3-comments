<?php

namespace Comments\Service;


use Comments\Models\CommentModel;
use Comments\Models\Entity\Comment;
use Comments\Models\Repository\CommentRepository;
use Comments\Service\CommentFactory\CommentModelFactory;
use Comments\SymfonyForms\CommentSendForm;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Zend\Mvc\Controller\Plugin\Redirect;

class CommentService
{

    /**
     * @var CommentViewService
     */
    private $commentViewService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Redirect
     */
    private $urlManager;

    public function __construct($commentViewService, $em, $urlManager)
    {
        $this->commentViewService = $commentViewService;
        $this->em = $em;

        $this->urlManager = $urlManager;

        $this->getCommentServiceView()->setJs('js/comment.js');
        $this->getCommentServiceView()->setCss('css/comments.css');
    }

    public function render()
    {
        $em = $this->getEntityManager();
        $commentsRep = $em
            ->getRepository(Comment::class)
            ->findAllParents();

        $countsComment = $em
            ->getRepository(Comment::class)
            ->countAllComments();

        /**
         * @var $request Request;
         */
        $request = $this->getCommentServiceView()->getSyRequest();

        /**
         * Creating Form
         */
        $form = $this
            ->getCommentServiceView()
            ->getFormBuilder()
            ->createBuilder(CommentSendForm::class)
            ->getForm()
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $commentEntity Comment
             * @var $parentEntity Comment
             */
            $commentEntity = $form->getData();
            $parentID = $form->get('parent')->getData();


            if (!is_null($parentID)) {
                $parentEntity = $this
                    ->getEntityManager()
                    ->getRepository(Comment::class)
                    ->find($parentID);
                $commentEntity->setParent($parentEntity);
            }
            $commentEntity->setIP($request->getClientIp());
            $commentEntity->setBrowserInformation($request->headers->get('User-Agent'));

            try{

                $this->getEntityManager()
                    ->persist($commentEntity);
                $this->getEntityManager()
                    ->flush();

                $this->getUrlManager()->toRoute('comments', ['action' => 'index']);

            } catch (ORMException $exception) {
                dump('ORM EXCEPTION - '. $exception->getMessage());
            }

        }

        $parentID = $form->get('parent')->getData();

        $parentEntity = null;
        if (!is_null($parentID)) {
            /**
             * @var $parentEntityDoc Comment
             */
            $parentEntityDoc = $this
                ->getEntityManager()
                ->getRepository(Comment::class)
                ->find($parentID);
            $parentEntity = new CommentModel($parentEntityDoc);
        }


        $form = $form->createView();

        /**
         * Creating ModelComment for Comment Entity
         */
        $factoryCommentModel = [];
        foreach ($commentsRep as $comment) {
            $f = new CommentModelFactory();
            $factoryCommentModel[] = $f->createComment($comment);
        }

        $this->getCommentServiceView()->renderViewComment($factoryCommentModel, $countsComment, $form, $parentEntity);
    }

    /**
     * @return Redirect
     */
    public function getUrlManager()
    {
        return $this->urlManager;
    }

    /**
     * @return CommentViewService
     */
    public function getCommentServiceView()
    {
        return $this->commentViewService;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

}