<?php

namespace Comments\Service\CommentFactory;




use Comments\Interfaces\ICommentModelFactory;
use Comments\Models\CommentModel;
use Comments\Models\Entity\Comment;

class CommentModelFactory implements ICommentModelFactory
{

    public function createComment(Comment $comment)
    {
        return new CommentModel($comment);
    }

}