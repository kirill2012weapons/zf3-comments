<?php

namespace Comments\Service\CommentFactory;


use Comments\Service\CommentViewService;
use Interop\Container\ContainerInterface;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Validator\Validation;
use Zend\Form\Annotation\Validator;
use Zend\ServiceManager\Factory\FactoryInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\HttpFoundation\Request;

class CommentFactoryView implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelperManager = $container->get('ViewHelperManager');

        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $headScript = $viewHelperManager->get('headScript');
        $headLink = $viewHelperManager->get('headLink');

        $formBuilder = Forms::createFormFactoryBuilder()
            ->addExtension(new ValidatorExtension($validator))
            ->addExtension(new HttpFoundationExtension())
            ->getFormFactory();

        $requestSy = Request::createFromGlobals();

        return new CommentViewService($headScript, $headLink, $formBuilder, $requestSy);
    }

}