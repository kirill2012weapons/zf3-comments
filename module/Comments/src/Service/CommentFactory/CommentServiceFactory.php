<?php

namespace Comments\Service\CommentFactory;


use Comments\Service\CommentService;
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\Plugin\Redirect;
use Zend\ServiceManager\Factory\FactoryInterface;

class CommentServiceFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $commentViewService = $container->get('comment_view');
        $em = $container->get('doctrine.entitymanager.orm_default');
        $urlManager = $container->get('ControllerPluginManager')->get('Redirect');
        return new CommentService($commentViewService, $em, $urlManager);
    }

}