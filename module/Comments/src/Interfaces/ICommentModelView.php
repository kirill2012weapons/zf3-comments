<?php

namespace Comments\Interfaces;

interface ICommentModelView
{
    public function render($form);
}