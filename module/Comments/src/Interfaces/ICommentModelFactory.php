<?php

namespace Comments\Interfaces;

use Comments\Models\Entity\Comment;

interface ICommentModelFactory
{
    public function createComment(Comment $comment);
}