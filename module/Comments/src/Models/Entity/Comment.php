<?php

namespace Comments\Models\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Comments\Models\Repository\CommentRepository")
 * @ORM\Table(name="tblcomments")
 * @ORM\HasLifecycleCallbacks
 */
class Comment
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="user_name",
     *     length=50,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Nick must not be blank, bro."
     * )
     * @Assert\Length(
     *     min="3",
     *     max="50",
     *     minMessage="More then 3 letters.",
     *     maxMessage="Less then 50 letters."
     * )
     * @Assert\Regex(
     *     pattern="/^([a-zA-Z])[a-zA-Z0-9_-]{3,50}$/",
     *     message="{{ value }} - Not Write. Must contain A-z with '_' or '-' and number, more then 3 and less then 50. Must start with Letter"
     * )
     */
    private $user_name;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="email",
     *     length=100,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Email must not be blank, bro."
     * )
     * @Assert\Length(
     *     max="100",
     *     maxMessage="Less then 100 letters."
     * )
     * @Assert\Email(
     *     message="Must be like - example.ur-email@example.com, {{ value }} - not correct! "
     * )
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(
     *     type="text",
     *     name="comment",
     *     length=5000,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Comment must not be blank, bro."
     * )
     * @Assert\Length(
     *     max="5000",
     *     maxMessage="Less then 5000 letters."
     * )
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="ip_adress",
     *     length=39,
     *     nullable=false
     * )
     */
    private $ip;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="browser_information",
     *     length=255,
     *     nullable=true
     * )
     */
    private $browser_information;

    /**
     * @var string
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false
     * )
     */
    private $created;

    /**
     * @var string
     * @ORM\Column(
     *     type="datetime",
     *     nullable=true
     * )
     */
    private $modified = null;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Comment",
     *     mappedBy="parent"
     * )
     */
    private $children;

    /**
     * @var Comment
     * @ORM\ManyToOne(
     *     targetEntity="Comment",
     *     inversedBy="children"
     * )
     * @ORM\JoinColumn(
     *     name="parrent_id",
     *     referencedColumnName="id"
     * )
     */
    private $parent;

    public function getID()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function setUserName($userName)
    {
        $this->user_name = $userName;
    }

    public function getUserName()
    {
        return $this->user_name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setIP($ip)
    {
        $this->ip = $ip;
    }

    public function getIP()
    {
        return $this->ip;
    }

    public function setBrowserInformation($browserInformation)
    {
        $this->browser_information = $browserInformation;
    }

    public function getBrowserInformation()
    {
        return $this->browser_information;
    }

    public function setParent(Comment $parent)
    {
        $this->parent = $parent;
        $parent->setChildren($this);
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setChildren(Comment $comment)
    {
        $this->children->add($comment);
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setModified(\DateTime $dateTime)
    {
        $this->modified = $dateTime;
    }

    public function getModified()
    {
        return $this->modified;
    }

    public function setCreated(\DateTime $dateTime)
    {
        $this->created = $dateTime;
    }

    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function timeStamp()
    {
        try {
            $this->setCreated(new \DateTime(date('Y-m-d H:i:s')));
            if ($this->getModified() == null) $this->setModified(new \DateTime(date('Y-m-d H:i:s')));
        } catch (\Exception $exception) {
            $log = new Logger();
            $wr = new Stream(getcwd() . '/logs/server_log/error.log');
            $log->addWriter($wr);
            $log->crit($exception->getMessage());
        }
    }

}