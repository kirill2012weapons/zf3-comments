<?php

namespace Comments\Models;


use Comments\Interfaces\ICommentModelView;
use Comments\Models\Entity\Comment;
use Comments\Service\CommentFactory\CommentFactory;
use Comments\Service\CommentFactory\CommentFactoryView;
use Comments\Service\CommentFactory\CommentModelFactory;
use Doctrine\Common\Collections\ArrayCollection;

class CommentModel implements ICommentModelView
{

    const PATH_TO_VIEW_COMMENT = __DIR__ . './../../view/comments/comments_template/comment_single.phtml';

    /**
     * @var array
     */
    public $comments;

    /**
     * @var CommentModel
     */
    public $activeComment = null;

    /**
     * @var Comment
     */
    public $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
        foreach ($comment->getChildren() as $child) {
            $cmFactory = new CommentModelFactory();
            $this->comments[] = $cmFactory->createComment($child);
        }
    }

    public function pushComment(ICommentModelView $commentModelView)
    {
        $this->comments[] = $commentModelView;
    }

    public function render($form)
    {
        include (self::PATH_TO_VIEW_COMMENT);
    }

    public function getComment()
    {
        return $this->comment->getComment();
    }

    public function getID()
    {
        return $this->comment->getID();
    }

    public function getTerminator()
    {
        /**
         * @var $created \DateTime
         */
        $created = $this->comment->getCreated();
        $dateNow = new \DateTime();
        $data = explode('-', date_diff($created, $dateNow)->format('%d-%h-%i'));
        $outputStr = '';
        if ($data[0] != 0) $outputStr .= $data[0] . ' days ';
        if ($data[1] != 0) $outputStr .= $data[1] . ' hours ';
        if ($data[2] != 0) $outputStr .= $data[2] . ' minutes ';
        if (empty($outputStr)) $outputStr = '0 second ';
        return $outputStr;
    }

    public function getUserName()
    {
        return $this->comment->getUserName();
    }

    public function getCountComments()
    {

    }

}