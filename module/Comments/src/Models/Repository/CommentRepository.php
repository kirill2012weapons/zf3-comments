<?php
/**
 * Created by PhpStorm.
 * User: kiril
 * Date: 01.04.2019
 * Time: 16:14
 */

namespace Comments\Models\Repository;


use Comments\Models\Entity\Comment;
use Doctrine\ORM\EntityRepository;

class CommentRepository extends EntityRepository
{
    public function findAll()
    {
        return parent::findAll();
    }

    public function findAllParents()
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('comm')
            ->from(Comment::class, 'comm')
            ->where('comm.parent IS NULL')
            ->orderBy('comm.created', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function countAllComments()
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('count(comm.id) as count')
            ->from(Comment::class, 'comm')
            ->getQuery()
            ->getSingleScalarResult();
    }
}