<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Comments;

use Comments\Controller\CommentController;
use Comments\Controller\Factory\Controller\ControllerFactory;
use Comments\Service\CommentFactory\CommentFactoryView;
use Comments\Service\CommentFactory\CommentServiceFactory;
use Comments\Service\CommentService;
use Comments\Service\CommentViewService;
use Comments\Service\HelloService;

return [

    /* DOCTRINE CONFIGURATIONS */
    'doctrine' => [
        'driver' => [
            'comments_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . './../src/Models/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Comments\Models\Entity' => 'comments_driver',
                ],
            ],
        ],
        'connection' => [
            // default connection name
            'orm_default' => [
                'driverClass' => \Doctrine\DBAL\Driver\PDOMySql\Driver::class,
                'params' => [
                    'host' => getenv('COMMENTS_DOCTRINE_HOST'),
                    'port' => getenv('COMMENTS_DOCTRINE_POST'),
                    'user' => getenv('COMMENTS_DOCTRINE_USER'),
                    'password' => getenv('COMMENTS_DOCTRINE_PASSWORD'),
                    'dbname' => getenv('COMMENTS_DOCTRINE_DBNAME'),
                ],
            ],
        ],
        'comments_driver' => [
            'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
            'cache' => 'array',
            'paths' => [
                __DIR__ . './../src/Models/Entity',
            ],
        ],
        'orm_default' => [
            'drivers' => [
                'Comments\Models\Entity' => 'comments_driver',
            ],
        ],
    ],


    /* CONTROLLER CONFIGURATIONS */
    'controllers' => [
        'factories' => [
            CommentController::class => ControllerFactory::class,
        ],
        'aliases' => [
            'Comments\Controller\Comment' => CommentController::class,
        ],
    ],


    /* ROUTE CONFIGURATION */
    'router' => [
        'routes' => [
            'comments' => [
                'type' => 'literal',
                'options' => [
                    'route' => '/comments',
                    'defaults' => [
                        'controller' => 'Comments\Controller\Comment',
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],


    /* VIEW CONFIGURATION */
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            'comments' => __DIR__ . '/../view',
        ],
    ],

    /* SERVICE MANAGER */
    'service_manager' => [
        'factories' => [
            CommentViewService::class => CommentFactoryView::class,
            CommentService::class => CommentServiceFactory::class,
        ],
        'aliases' => [
            'em' => 'Doctrine\ORM\EntityManager',
            'comment_view' => CommentViewService::class,
            'comment_service' => CommentService::class,
        ],
    ],
];
