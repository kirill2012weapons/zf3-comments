# ZendSkeletonApplication

## Install ENV

1) 
```bash
git clone
```
2) Copy 
```bash
\{{PROJ_ROOT}}\config\.env.example 
```
to 
```bash
\{{PROJ_ROOT}}\config\.env
```
with data to ur data base

3) update composer
```bash
composer update
```

## Migrations to database

From the root
```bash
.\vendor\bin\doctrine-migrations migrations:migrate - IF Windows
/vendor/bin/doctrine-migrations migrations:migrate - IF Linux
```
Press - Y; Install Migrations.

## Comments Work!

```php
$this
    ->getServiceManager()
    ->get('comment_service'),
```

Config Apache
```apacheconfig
<VirtualHost {{LOCAL_HOST}}:80>
     ServerName {{LOCAL_HOST}}
     DocumentRoot "{{PATH_TO_PROJ}}/{{PROJ_NAME}}/public"
     SetEnv APPLICATION_ENV "development"
     <Directory "{{PATH_TO_PROJ}}/{{PROJ_NAME}}/public">
         DirectoryIndex index.php
         AllowOverride All
         Order allow,deny
         Allow from all
     </Directory>
</VirtualHost>
```

config .htaccess
```apacheconfig
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} -s [OR]
RewriteCond %{REQUEST_FILENAME} -l [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^.*$ - [L]
RewriteCond %{REQUEST_URI}::$1 ^(/.+)/(.*)::\2$
RewriteRule ^(.*) - [E=BASE:%1]
RewriteRule ^(.*)$ %{ENV:BASE}/index.php [L]

```
