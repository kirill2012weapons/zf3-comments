<?php

require_once __DIR__ . "./../vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\ORMException;
use Symfony\Component\Dotenv\Dotenv;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationRegistry;

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'./../config/.env');

$paths = [
    __DIR__ . './../module/Comments/src/Models/Entity'
];
$isDevMode = true;

$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => getenv('COMMENTS_DOCTRINE_USER'),
    'password' => getenv('COMMENTS_DOCTRINE_PASSWORD'),
    'dbname'   => getenv('COMMENTS_DOCTRINE_DBNAME'),
    'host'   => getenv('COMMENTS_DOCTRINE_HOST'),
);

$config = Setup::createConfiguration($isDevMode);
try {
    $driver = new AnnotationDriver( new AnnotationReader(), $paths );
} catch (AnnotationException $e) {
    echo 'ERROR : ' . $e->getMessage();
    return;
}

AnnotationRegistry::registerLoader('class_exists');

$config->setMetadataDriverImpl($driver);

try {
    $entityManager = EntityManager::create($dbParams, $config);
} catch (ORMException $e) {
    echo 'ERROR : ' . $e->getMessage();
    return;
}

return ConsoleRunner::createHelperSet($entityManager);