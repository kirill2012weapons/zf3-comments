<?php

declare(strict_types=1);

namespace Comments\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190329161419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tblcomments (id INT AUTO_INCREMENT NOT NULL, parrent_id INT DEFAULT NULL, user_name VARCHAR(50) NOT NULL, email VARCHAR(100) NOT NULL, comment TEXT NOT NULL, ip_adress VARCHAR(39) NOT NULL, browser_information VARCHAR(255) DEFAULT NULL, created DATETIME NOT NULL, modified DATETIME DEFAULT NULL, INDEX IDX_70857F23C83F53CA (parrent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tblcomments ADD CONSTRAINT FK_70857F23C83F53CA FOREIGN KEY (parrent_id) REFERENCES tblcomments (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tblcomments DROP FOREIGN KEY FK_70857F23C83F53CA');
        $this->addSql('DROP TABLE tblcomments');
    }
}
