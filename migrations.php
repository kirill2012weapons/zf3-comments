<?php

return [
    'name' => 'Migrations For Comments',
    'migrations_namespace' => 'Comments\Migrations',
    'table_name' => 'doctrine_migration_versions',
    'column_name' => 'version',
    'column_length' => 255,
    'executed_at_column_name' => 'executed_at',
    'migrations_directory' => '/data/Migrations/Classes',
    'all_or_nothing' => true,
];